#include "StdAfx.h"
#include "EDResEditDlg.h"

int Show_ResEditDlg(LPCWSTR lpResTag, bool bMultiFiles, LPCWSTR lpResName, HWND hWnd, CStringW& strOutTag, CStringW& strOutName, bool bDisableCombobox)
{
	DMSmartPtrT<EDResEditDlg> pDlg; 
	pDlg.Attach(new EDResEditDlg());
	if (NULL == hWnd)
	{
		hWnd = g_pMainWnd->m_hWnd;
	}

	pDlg->m_strNameText = lpResName;
	pDlg->m_strTag = lpResTag;
	pDlg->m_bDisableCombobox = bDisableCombobox;
	pDlg->m_bMultiFiles = bMultiFiles;
	int iRet = pDlg->DoModal(L"ds_reseditdlg", hWnd);
	strOutTag = pDlg->m_strTag;
	strOutName = pDlg->m_strNameText;
 	if (g_pMainWnd && g_pMainWnd->IsWindow())
 	{
 		g_pMainWnd->SetActiveWindow();
 	}
	return iRet;
}

BEGIN_MSG_MAP(EDResEditDlg)
	MSG_WM_INITDIALOG(OnInitDialog) 
	MSG_WM_SIZE(OnSize)
	CHAIN_MSG_MAP(DMHDialog)
END_MSG_MAP()
BEGIN_EVENT_MAP(EDResEditDlg)
	EVENT_NAME_COMMAND(L"closebutton", OnBtnClose)
	EVENT_NAME_COMMAND(L"ds_cancelbtn", OnBtnClose)
	EVENT_NAME_COMMAND(L"ds_okbtn", OnBtnOk)
END_EVENT_MAP()

EDResEditDlg::EDResEditDlg() :m_bDisableCombobox(false), m_bMultiFiles(false)
{
}  

BOOL EDResEditDlg::OnInitDialog(HWND wndFocus, LPARAM lInitParam)
{
	DUIComboBox* pCombobox = FindChildByNameT<DUIComboBox>(L"ds_ResTagCombobox"); DMASSERT(pCombobox);
	if (pCombobox)
	{
		pCombobox->SetCurSel(m_strTag == TAG2DSTICKER ? 0 : 1);
		pCombobox->SetAttribute(L"bdisable", m_bDisableCombobox ? L"1" : L"0");
		pCombobox->SetAttribute(L"clrbg", m_bDisableCombobox ? L"rgba(28, 2d, 32, FF)" : L"rgba(40, 4b, 57, FF)"); //clrbgdisable 好像无效
	}

	m_pNameEdit = FindChildByNameT<DUIRichEdit>(L"ds_editnametext"); DMASSERT(m_pNameEdit);
	if (m_pNameEdit)
	{
		m_pNameEdit->SetWindowTextW(m_strNameText);
	} 
	return TRUE;
}

void EDResEditDlg::OnSize(UINT nType, CSize size)
{
	if (!IsIconic()) 
	{
		CRect rcWnd;
		::GetWindowRect(m_hWnd, &rcWnd);
		::OffsetRect(&rcWnd, -rcWnd.left, -rcWnd.top);  
		HRGN hWndRgn = ::CreateRoundRectRgn(0, 0, rcWnd.right, rcWnd.bottom,4,4);
		SetWindowRgn(hWndRgn, TRUE);
		::DeleteObject(hWndRgn); 
	}             
	SetMsgHandled(FALSE);
}

DMCode EDResEditDlg::OnBtnClick(int uID)
{
	EndDialog(uID);
	if (g_pMainWnd && g_pMainWnd->IsWindow())
	{
		g_pMainWnd->SetActiveWindow();
	}
	return DM_ECODE_OK;
}

DMCode EDResEditDlg::OnBtnClose()
{
	return OnBtnClick(IDCANCEL);
}

DMCode EDResEditDlg::OnBtnOk()
{
	m_strTag = TAG2DSTICKER;
	DUIComboBox* pCombobox = FindChildByNameT<DUIComboBox>(L"ds_ResTagCombobox"); DMASSERT(pCombobox);
	if (pCombobox)
	{
		m_strTag = pCombobox->GetCurSel() == 0 ? TAG2DSTICKER : TAGMAKEUPNODE;
	}
	
	m_strNameText = m_pNameEdit->GetWindowTextW();

	CStringW strOutText;
	if (!EDResourceParser::IsResourceNameCharacterValid(m_strNameText, m_strTag, m_bMultiFiles, strOutText))
	{
		ED_MessageBox(strOutText, MB_OK, L"命名错误", m_hWnd);
		return DM_ECODE_FAIL;
	}

	if (EDResourceParser::sta_pResourceParser)
	{
		if (EDResourceParser::sta_pResourceParser->IsResourceNameSameConflict(m_strTag, m_strNameText))
		{
			ED_MessageBox(L"同类型资源发生重名", MB_OK, L"重命名错误", m_hWnd);
			return DM_ECODE_FAIL;
		}
	}
	return OnBtnClick(IDOK);
}