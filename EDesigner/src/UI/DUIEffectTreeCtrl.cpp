#include "StdAfx.h"
#include "DUIEffectTreeCtrl.h"

namespace ED
{
	DUIEffectTreeCtrl::DUIEffectTreeCtrl() :m_hOldSelectItem(NULL)
	{
		m_EventMgr.SubscribeEvent(DM::DMEventTCSelChangedArgs::EventID, Subscriber(&DUIEffectTreeCtrl::OnTreeItemSelectChanged, this));
	}

	DUIEffectTreeCtrl::~DUIEffectTreeCtrl()
	{
	}

	///< 删除一个节点，不FreeNode
	void DUIEffectTreeCtrl::DeleteItemNoFree(HDMTREEITEM hItem)
	{
		HDMTREENODE hsNode = (HDMTREENODE)hItem;
		DMASSERT(hsNode);
		if (hsNode == DMTVN_ROOT)
		{
			//FreeNode(DMTVN_ROOT);
			m_hRootFirst = NULL;
			m_hRootLast = NULL;
			return;
		}
		DMTREENODE nodeCopy = *hsNode;
		bool bRootFirst = hsNode == m_hRootFirst;
		bool bRootLast = hsNode == m_hRootLast;
		//FreeNode(hsNode);

		if (nodeCopy.hPrevSibling)///<has prevsibling
		{
			nodeCopy.hPrevSibling->hNextSibling = nodeCopy.hNextSibling;
		}
		else if (nodeCopy.hParent)///<parent's first child
		{
			nodeCopy.hParent->hChildFirst = nodeCopy.hNextSibling;
		}
		if (nodeCopy.hNextSibling)///< update next sibling's previous sibling
		{
			nodeCopy.hNextSibling->hPrevSibling = nodeCopy.hPrevSibling;
		}
		else if (nodeCopy.hParent)///<parent's last child
		{
			nodeCopy.hParent->hChildLast = nodeCopy.hPrevSibling;
		}
		///<update root item
		if (bRootFirst)
		{
			m_hRootFirst = nodeCopy.hNextSibling;
		}
		if (bRootLast)
		{
			m_hRootLast = nodeCopy.hPrevSibling;
		}
	}

	HDMTREEITEM DUIEffectTreeCtrl::InsertItemNoData(HDMTREEITEM hInsertItem, HDMTREEITEM hParent /*= DMTVI_ROOT*/, HDMTREEITEM hInsertAfter /*= DMTVI_LAST*/)
	{
		HDMTREENODE hParentNode = (HDMTREENODE)hParent;
		HDMTREENODE hInsertAfterNode = (HDMTREENODE)hInsertAfter;
		if (hParentNode == DMTVN_ROOT)
		{
			hParentNode = NULL;
		}
		DMASSERT(hInsertAfter);
		if (hInsertAfterNode != DMTVN_FIRST && hInsertAfterNode != DMTVN_LAST)
		{
			if (hInsertAfterNode->hParent != hParentNode)
			{
				return NULL;
			}
			if (hInsertAfterNode->hNextSibling == NULL)
			{
				hInsertAfterNode = DMTVN_LAST;
			}
		}

		HDMTREENODE hInserted = (HDMTREENODE)hInsertItem;
	//	hInserted->data = data;
		hInserted->hParent = hParentNode;
		hInserted->hChildFirst = NULL;
		hInserted->hChildLast = NULL;

		if (hInsertAfterNode == DMTVN_FIRST)
		{
			hInserted->hPrevSibling = NULL;
			if (hParentNode == NULL)///<root
			{
				hInserted->hNextSibling = m_hRootFirst;
				if (m_hRootFirst)
				{
					m_hRootFirst->hPrevSibling = hInserted;
				}
				m_hRootFirst = hInserted;
				if (m_hRootLast == NULL)
				{
					m_hRootLast = hInserted;
				}
			}
			else ///<has parent
			{
				hInserted->hNextSibling = hParentNode->hChildFirst;
				if (hInserted->hNextSibling)
				{
					hInserted->hNextSibling->hPrevSibling = hInserted;
					hParentNode->hChildFirst = hInserted;
				}
				else
				{
					hParentNode->hChildLast = hParentNode->hChildFirst = hInserted;
				}
			}
		}
		else if (hInsertAfterNode == DMTVN_LAST)
		{
			hInserted->hNextSibling = NULL;
			if (hParentNode == NULL)///<root
			{
				hInserted->hPrevSibling = m_hRootLast;
				if (m_hRootLast)
				{
					m_hRootLast->hNextSibling = hInserted;
				}
				m_hRootLast = hInserted;
				if (!m_hRootFirst)
				{
					m_hRootFirst = hInserted;
				}
			}
			else
			{
				hInserted->hPrevSibling = hParentNode->hChildLast;
				if (hParentNode->hChildLast)
				{
					hInserted->hPrevSibling->hNextSibling = hInserted;
					hParentNode->hChildLast = hInserted;
				}
				else
				{
					hParentNode->hChildLast = hParentNode->hChildFirst = hInserted;
				}
			}
		}
		else
		{
			HDMTREENODE hNextSibling = hInsertAfterNode->hNextSibling;
			hInserted->hPrevSibling = hInsertAfterNode;
			hInserted->hNextSibling = hNextSibling;
			hNextSibling->hPrevSibling = hInsertAfterNode->hNextSibling = hInserted;
		}
		return (HDMTREEITEM)hInserted;
	}

	HDMTREEITEM DUIEffectTreeCtrl::MoveItemToNewPos(HDMTREEITEM hMoveItem, HDMTREEITEM hParent /*= DMTVI_ROOT*/, HDMTREEITEM hInsertAfter /*= DMTVI_LAST*/)
	{
		DeleteItemNoFree(hMoveItem);
		HDMTREEITEM hItem = InsertItemNoData(hMoveItem, hParent, hInsertAfter);
		UpdateVisibleMap();
		DM_Invalidate();
		return hItem;
	}

	void DUIEffectTreeCtrl::OnRButtonDown(UINT nFlags, CPoint pt)
	{
		do
		{
			if (!m_bRightClickSel)
			{
				break;
			}
			m_hHoverItem = HitTest(pt);
			if (m_hHoverItem != m_hSelItem
				&& m_hHoverItem)
			{
				INSERTNEWOBSERVEACTION(new TreeSelectActionSlot(m_hSelItem, m_hHoverItem, this));
				SelectItem(m_hHoverItem, false);
			}

			// 是否弹出菜单
			if (NULL == m_hHoverItem || DMTVI_ROOT == m_hHoverItem || NULL == g_pMainWnd)
			{
				break;
			}

			DM::LPTVITEMEX pData = GetItem(m_hHoverItem);
			if (!pData)
				break;
			DUIRichEdit* pItemEdit = pData->pPanel->FindChildByNameT<DUIRichEdit>(TREEITEMEDIT, true); DMASSERT(pItemEdit);
			if (pItemEdit)
			{
				pItemEdit->DM_OnKillFocus();  //右键弹出的时候   取消重命名状态
			}

			g_pMainWnd->PopRBtnDownEffectTreeMenu(m_hHoverItem);
		} while (false);
	}

	DMCode DUIEffectTreeCtrl::OnTreeAddBtnClicked(DMEventArgs *pEvt)
	{
		do 
		{
			if (!pEvt)
				break;
			DUIButton* pTreeAddBtn = dynamic_cast<DUIButton*>(pEvt->m_pSender);
			if (!pTreeAddBtn)
				break;

			DUIWindow* pParent = pTreeAddBtn->DM_GetWindow(GDW_PARENT);
			DUIItemPanel* pPanel = dynamic_cast<DUIItemPanel*>(pParent);
			if (!pPanel)
				break;

			HDMTREEITEM hItem = (HDMTREEITEM)pPanel->GetItemId();
			if (!hItem || NULL == g_pMainWnd)
				break;

			g_pMainWnd->PopEffectTreeAddBtnMenu(hItem);
		} while (false);
		
		return DM_ECODE_OK;
	}

	HDMTREEITEM DUIEffectTreeCtrl::InsertRootItem(CStringW strItemName, EffectTempletType data, HDMTREEITEM hInsertAfter/*= DMTVI_LAST*/, bool bEnsureVisible/*= false*/, bool bSelectItem/*= false*/, HDMTREEITEM hParent)
	{
		CStringW strSkin = L"ds_facemakeupeffect";
		bool	 bCanAddChild = true;
		
		switch (data)
		{
		case EFFECTTEMPBTN_STICKERSMAJOR://stickers
		{
			strSkin = L"ds_2dstickereffcet";
		}
		break;
		case EFFECTTEMPBTN_TRANSITIONS://transitions
		{
			strSkin = L"ds_filtereffect";
		}
		break;
		case EFFECTTEMPBTN_2DSTICKERMAJOR://"2D贴纸"
		{
			strSkin = L"ds_2dstickereffcet";
		}
		break;
		case EFFECTTEMPBTN_FILTER: //"滤镜"
		{
			strSkin = L"ds_filtereffect";
			bCanAddChild = false;
		}
		break;
		case EFFECTTEMPBTN_HAIRCOLOR: //染发
		{
			strSkin = L"ds_haircoloreffect";
			bCanAddChild = false;
		}
		break;
		case EFFECTTEMPBTN_PORTRAITMAT: //抠出人像
		{
			strSkin = L"ds_portraiteffect";
			bCanAddChild = false;
		}
		break;
		case EFFECTTEMPBTN_MAKEUPS: //抠出人像
		{
			strSkin = L"ds_facemakeupeffect";
			bCanAddChild = true;
		}
		break;
		default:
			break;
		}

		DMXmlDocument Doc;
		DMXmlNode XmlItem = Doc.Base();
		XmlItem = XmlItem.InsertChildNode(L"treeitem");
		XmlItem.SetAttribute(L"bcollapsed", L"0"); XmlItem.SetAttribute(L"childoffset", L"7");
		if (hParent)
		{
			XmlItem.SetAttribute(L"childoffset", L"14");
		}
		DMXmlNode SkinXmlItem = XmlItem.InsertChildNode(L"static");
		SkinXmlItem.SetAttribute(L"pos", L"28,5,@16,@16"); SkinXmlItem.SetAttribute(L"skin", strSkin);
		if (hParent)
		{
			SkinXmlItem.SetAttribute(L"pos", L"42,5,@16,@16");
		}
		DMXmlNode StaXmlItem = XmlItem.InsertChildNode(L"static");
		StaXmlItem.SetAttribute(L"name", TREESTATEXT); StaXmlItem.SetAttribute(L"pos", L"48,0,-65,-0"); StaXmlItem.SetAttribute(L"text", strItemName);
		if (hParent)
		{
			StaXmlItem.SetAttribute(L"pos", L"62,0,-65,-0");
		}
		StaXmlItem.SetAttribute(L"clrtext", L"rgba(ff,ff,ff,dd)"); StaXmlItem.SetAttribute(L"align", L"left"); StaXmlItem.SetAttribute(L"alignv", L"center");
		StaXmlItem.SetAttribute(L"bdot", L"1"); StaXmlItem.SetAttribute(L"font", L"face:微软雅黑,size:12,weight:150");
		DMXmlNode EditXmlItem = XmlItem.InsertChildNode(L"richedit");
		EditXmlItem.SetAttribute(L"name", TREEITEMEDIT); EditXmlItem.SetAttribute(L"pos", L"46,4,-68,-4"); EditXmlItem.SetAttribute(L"skin", L"ds_editframe"); EditXmlItem.SetAttribute(L"maxbuf", L"100");
		if (hParent)
		{
			EditXmlItem.SetAttribute(L"pos", L"60,4,-68,-4");
		}
		EditXmlItem.SetAttribute(L"rcinsertmargin", L"2,1,2,0"); EditXmlItem.SetAttribute(L"clrcaret", L"pbgra(ff,ff,ff,ff)"); EditXmlItem.SetAttribute(L"clrtext", L"rgba(ff,ff,ff,ff)"); EditXmlItem.SetAttribute(L"bwantreturn", L"1");
		EditXmlItem.SetAttribute(L"bvisible", L"0"); EditXmlItem.SetAttribute(L"bautosel", L"1"); EditXmlItem.SetAttribute(L"font", L"face:微软雅黑,size:12,weight:200");
		DMXmlNode BtnXmlItem = XmlItem.InsertChildNode(L"button");
		BtnXmlItem.SetAttribute(L"name", TREEADDBUTTON); BtnXmlItem.SetAttribute(L"pos", L"-60,5,@16,@16"); BtnXmlItem.SetAttribute(L"cursor", L"hand"); BtnXmlItem.SetAttribute(L"skin", L"ds_treeaddeffectbutton");
		DMXmlNode ChkXmlItem = XmlItem.InsertChildNode(L"checkbox");
		ChkXmlItem.SetAttribute(L"name", TREECHECKNAME); ChkXmlItem.SetAttribute(L"pos", L"-35,6,@16,@16"); ChkXmlItem.SetAttribute(L"align", L"left"); ChkXmlItem.SetAttribute(L"cursor", L"hand"); ChkXmlItem.SetAttribute(L"checkskin", L"ds_eyecheckbox"); ChkXmlItem.SetAttribute(L"bchecked", L"1");
		DMXmlNode fixNailIcoXmlItem = XmlItem.InsertChildNode(L"window");
		fixNailIcoXmlItem.SetAttribute(L"name", FIXNAILICONAME); fixNailIcoXmlItem.SetAttribute(L"pos", L"-35,6,@16,@16"); fixNailIcoXmlItem.SetAttribute(L"skin", L"ds_fixnailico"); fixNailIcoXmlItem.SetAttribute(L"bvisible", L"0"); fixNailIcoXmlItem.SetAttribute(L"tiptext", L"位置已固定，不可上下调整");

		Init_Debug_XmlBuf(XmlItem);

		HDMTREEITEM hRet = InsertItem(XmlItem, hParent, hInsertAfter);
		if (bSelectItem && hRet) SelectItem(hRet, bEnsureVisible);
		return hRet;
	}

	HDMTREEITEM DUIEffectTreeCtrl::InsertChildItem(CStringW strItemName, HDMTREEITEM hParent, HDMTREEITEM hInsertAfter /*= DMTVI_LAST*/, bool bEnsureVisible /*= false*/, bool bSelectItem/*= false*/, CStringW strDefaultIcon /*= L"ds_effectnode"*/)
	{
		DMXmlDocument Doc;
		DMXmlNode XmlItem = Doc.Base();
		XmlItem = XmlItem.InsertChildNode(L"treeitem");
		DMXmlNode SkinXmlItem = XmlItem.InsertChildNode(L"static");
		SkinXmlItem.SetAttribute(L"pos", L"42,5,@16,@16"); SkinXmlItem.SetAttribute(L"skin", strDefaultIcon);
		if (GetParentItem(hParent))
		{
			SkinXmlItem.SetAttribute(L"pos", L"56,5,@16,@16");
		}
		DMXmlNode StaXmlItem = XmlItem.InsertChildNode(L"static");
		StaXmlItem.SetAttribute(L"name", TREESTATEXT); StaXmlItem.SetAttribute(L"pos", L"62,0,-65,-0"); StaXmlItem.SetAttribute(L"text", strItemName);
		if (GetParentItem(hParent))
		{
			StaXmlItem.SetAttribute(L"pos", L"76,0,-65,-0");
		}
		StaXmlItem.SetAttribute(L"clrtext", L"rgba(cd,dd,ef,ff)"); StaXmlItem.SetAttribute(L"align", L"left"); StaXmlItem.SetAttribute(L"alignv", L"center");
		StaXmlItem.SetAttribute(L"bdot", L"1"); StaXmlItem.SetAttribute(L"font", L"face:微软雅黑,size:12,weight:150");
		DMXmlNode EditXmlItem = XmlItem.InsertChildNode(L"richedit");
		EditXmlItem.SetAttribute(L"name", TREEITEMEDIT); EditXmlItem.SetAttribute(L"pos", L"60,4,-68,-4"); EditXmlItem.SetAttribute(L"skin", L"ds_editframe"); EditXmlItem.SetAttribute(L"maxbuf", L"100");
		if (GetParentItem(hParent))
		{
			EditXmlItem.SetAttribute(L"pos", L"74,4,-68,-4");
		}
		EditXmlItem.SetAttribute(L"rcinsertmargin", L"2,1,2,0"); EditXmlItem.SetAttribute(L"clrcaret", L"pbgra(ff,ff,ff,ff)"); EditXmlItem.SetAttribute(L"clrtext", L"rgba(ff,ff,ff,ff)"); EditXmlItem.SetAttribute(L"bwantreturn", L"1");
		EditXmlItem.SetAttribute(L"bvisible", L"0"); EditXmlItem.SetAttribute(L"bautosel", L"1"); EditXmlItem.SetAttribute(L"font", L"face:微软雅黑,size:12,weight:200");

		DMXmlNode ChkXmlItem = XmlItem.InsertChildNode(L"checkbox");
		ChkXmlItem.SetAttribute(L"name", TREECHECKNAME); ChkXmlItem.SetAttribute(L"pos", L"-35,6,@16,@16"); ChkXmlItem.SetAttribute(L"align", L"left"); ChkXmlItem.SetAttribute(L"cursor", L"hand"); ChkXmlItem.SetAttribute(L"checkskin", L"ds_eyecheckbox"); ChkXmlItem.SetAttribute(L"bchecked", L"1");
		DMXmlNode fixNailIcoXmlItem = XmlItem.InsertChildNode(L"window");
		fixNailIcoXmlItem.SetAttribute(L"name", FIXNAILICONAME); fixNailIcoXmlItem.SetAttribute(L"pos", L"-35,6,@16,@16"); fixNailIcoXmlItem.SetAttribute(L"skin", L"ds_fixnailico"); fixNailIcoXmlItem.SetAttribute(L"bvisible", L"0"); fixNailIcoXmlItem.SetAttribute(L"tiptext", L"位置已固定，不可上下调整");
		Init_Debug_XmlBuf(XmlItem);

		HDMTREEITEM hRet = InsertItem(XmlItem, hParent, hInsertAfter);
		if (bSelectItem)
		{
			INSERTNEWOBSERVEACTION(new TreeSelectActionSlot(m_hSelItem, hRet, this));
			SelectItem(hRet, false);
		}
		return hRet;
	}

	bool DUIEffectTreeCtrl::IsItemEyeChkAndNotDisable(HDMTREEITEM hItem)
	{
		return IsItemEyeChk(hItem) && !IsItemEyeDisable(hItem);
	}

	void DUIEffectTreeCtrl::TriggerClickItemEye(HDMTREEITEM hItem)
	{
		do 
		{
			if (NULL == hItem || DMTVI_ROOT == hItem)
			{
				break;
			}

			DM::LPTVITEMEX pData = GetItem(hItem);
			if (!pData)
				break;

			DUICheckBox* pTreeEyeChk = pData->pPanel->FindChildByNameT<DUICheckBox>(TREECHECKNAME, true); DMASSERT(pTreeEyeChk);
			if (!pTreeEyeChk)
				break;

			pTreeEyeChk->DM_SetCheck(!pTreeEyeChk->DM_IsChecked());
			DMEventCmdArgs args(pTreeEyeChk);
			pTreeEyeChk->m_EventMgr.FireEvent(args);
		} while (false);	
	}

	bool DUIEffectTreeCtrl::IsItemEyeChk(HDMTREEITEM hItem)
	{
		if (NULL == hItem || DMTVI_ROOT == hItem)
		{
			return false;
		}

		DM::LPTVITEMEX pData = GetItem(hItem);
		if (!pData)
			return false;
		DUICheckBox* pTreeEyeChk = pData->pPanel->FindChildByNameT<DUICheckBox>(TREECHECKNAME, true); DMASSERT(pTreeEyeChk);
		return pTreeEyeChk && pTreeEyeChk->DM_IsChecked();
	}

	bool DUIEffectTreeCtrl::SetItemEyeVisible(HDMTREEITEM hItem, bool bVisible)
	{
		if (NULL == hItem || DMTVI_ROOT == hItem)
		{
			return false;
		}

		DM::LPTVITEMEX pData = GetItem(hItem);
		if (!pData)
			return false;
		DUICheckBox* pTreeEyeChk = pData->pPanel->FindChildByNameT<DUICheckBox>(TREECHECKNAME, true); DMASSERT(pTreeEyeChk);
		if (pTreeEyeChk)
			pTreeEyeChk->SetAttribute(L"bvisible", bVisible ? L"1" : L"0");
		return true;
	}

	//ds_fixnailico
 	bool DUIEffectTreeCtrl::SetFixnailiconVisible(HDMTREEITEM hItem, bool bVisible)
 	{
 		if (NULL == hItem || DMTVI_ROOT == hItem)
 		{
 			return false;
 		}
 
 		DM::LPTVITEMEX pData = GetItem(hItem);
 		if (!pData)
 			return false;
		DUIWindow* pFixnailiconWnd = pData->pPanel->FindChildByNameT<DUIWindow>(FIXNAILICONAME, true); DMASSERT(pFixnailiconWnd);
		if (pFixnailiconWnd)
			pFixnailiconWnd->SetAttribute(L"bvisible", bVisible ? L"1" : L"0");
 		return true;
 	}

	bool DUIEffectTreeCtrl::IsItemEyeDisable(HDMTREEITEM hItem)
	{
		if (NULL == hItem || DMTVI_ROOT == hItem)
		{
			return false;
		}

		DM::LPTVITEMEX pData = GetItem(hItem);
		if (!pData)
			return false;
		DUICheckBox* pTreeEyeChk = pData->pPanel->FindChildByNameT<DUICheckBox>(TREECHECKNAME, true); DMASSERT(pTreeEyeChk);
		return pTreeEyeChk->DM_IsDisable();
	}

	DMCode DUIEffectTreeCtrl::OnTreeEyeBtnClicked(DMEventArgs *pEvt)
	{		
		HDMTREEITEM hItem = m_hHoverItem;
		SetTreeItemEyeBtnChk(hItem, IsItemEyeChk(hItem), false);

		if (hItem == m_hSelItem || NULL == GetParentItem(hItem)) //点击当前选中项的眼睛时 或者 点击root项时  需要刷新DragFrame
		{
			g_pMainWnd->m_pObjEditor->DragFrameInSelMode();
		}
		return DM_ECODE_OK;
	}

	DMCode DUIEffectTreeCtrl::OnTreeItemEditKillFocus(DMEventArgs* pEvent)
	{
		DMCode iErr = DM_ECODE_FAIL;
		do
		{
			DMEventRENotifyArgs *pEvt = (DMEventRENotifyArgs*)pEvent;
			if (!pEvt)
				break;
			static bool bRetry = false;
			if (EN_KILLFOCUS == pEvt->m_iNotify && false == bRetry)
			{
				DUIRichEdit* pTreeItemEdit = (DUIRichEdit*)pEvt->m_pSender;
				if (!pTreeItemEdit)
					break;
				DUIWindow* pParent = pTreeItemEdit->DM_GetWindow(GDW_PARENT);
				DUIItemPanel* pPanel = dynamic_cast<DUIItemPanel*>(pParent);
				if (!pPanel)
					break;
				
				DUIStatic* pStatic = pPanel->FindChildByNameT<DUIStatic>(TREESTATEXT);
				CStringW strNewName = pTreeItemEdit->GetWindowText();
				if (!strNewName.IsEmpty() && pStatic)
				{
					CStringW strOldName = pStatic->m_pDUIXmlInfo->m_strText;
					if (strNewName.Compare((LPCTSTR)strOldName) != 0)
					{
						HDMTREEITEM hItemRenamed = NULL;
						if (NULL != m_hOldSelectItem && DMTVI_ROOT != m_hOldSelectItem)
						{
							DM::LPTVITEMEX data = GetItem(m_hOldSelectItem);
							if (data->pPanel == pPanel)
							{
								hItemRenamed = m_hOldSelectItem;
							}
						}
						if (hItemRenamed == NULL && NULL != m_hSelItem && DMTVI_ROOT != m_hSelItem)
						{
							DM::LPTVITEMEX data = GetItem(m_hSelItem);
							if (data->pPanel == pPanel)
							{
								hItemRenamed = m_hSelItem;
							}
						}

						if (EDResourceParser::IsNameOnlyContainNumLettersUnderline(strNewName))
						{
							pStatic->DV_SetWindowText(strNewName);
							ObjTreeDataPtr pData = (ObjTreeDataPtr)GetItemData(m_hOldSelectItem ? m_hOldSelectItem : m_hSelItem); DMASSERT(pData && pData->m_pJsonParser);
							if (pData && pData->m_pJsonParser)
							{
								pData->m_pJsonParser->SetJSonMemberKey(std::string(EDBASE::w2a(strNewName)).c_str());
								pData->m_pJsonParser->MarkDataDirty();
								g_pMainWnd->OnUpdateSettingPanelInfo(pData->m_pJsonParser, true);
							}
						}
						else
						{
							ED_MessageBox(L"Transition名称只能包含数字、字母和下划线", MB_OK, L"重命名错误", g_pMainWnd->m_hWnd);
						}						
					}
				}
				bRetry = true;
				pTreeItemEdit->DM_SetVisible(false, true);// 此函数会引发多次进入EN_KILLFOCUS，所以加判断

				if (pStatic)
				{
					pStatic->DM_SetVisible(true, true);
				}
				bRetry = false;
				iErr = DM_ECODE_OK;
			}
		} while (false);
		return iErr;
	}

	DMCode DUIEffectTreeCtrl::OnTreeItemEditInputReturn(DMEventArgs* pEvent)
	{
		DUIRichEdit* pTreeItemEdit = (DUIRichEdit*)pEvent->m_pSender;
		if (!pTreeItemEdit)
			return DM_ECODE_FAIL;
		if (pTreeItemEdit) pTreeItemEdit->DM_OnKillFocus();
		return DM_ECODE_OK;
	}

	DMCode DUIEffectTreeCtrl::OnTreeItemSelectChanged(DMEventArgs* pEvet)
	{
		DMCode iRet = DM_ECODE_FAIL;
		do 
		{
			DM::DMEventTCSelChangedArgs *pEvent = (DM::DMEventTCSelChangedArgs*)pEvet;
			if (!pEvent || !pEvent->m_hOldSel || DMTVI_ROOT == pEvent->m_hOldSel)
				break;
			m_hOldSelectItem = pEvent->m_hOldSel;
			DM::LPTVITEMEX pData = GetItem(pEvent->m_hOldSel);
			if (!pData)
				break;
			DUIRichEdit* pItemEdit = pData->pPanel->FindChildByNameT<DUIRichEdit>(TREEITEMEDIT, true); DMASSERT(pItemEdit);
			if (!pItemEdit)
				break;
			pItemEdit->DM_OnKillFocus();
			if(GetSelectedItem())
				DV_SetFocusWnd();
			iRet = DM_ECODE_OK;
		} while (false);
		return iRet;
	}

	DMCode DUIEffectTreeCtrl::RenameTreeItem(HDMTREEITEM hItem)
	{
		DMCode hRet = DM_ECODE_FAIL;
		do 
		{
			if (NULL == hItem || DMTVI_ROOT == hItem)
			{
				break;
			}

			DM::LPTVITEMEX pData = GetItem(hItem);
			if (!pData)
				break;

			DUIStatic* pStaticText = pData->pPanel->FindChildByNameT<DUIStatic>(TREESTATEXT, true); DMASSERT(pStaticText);
			DUIRichEdit* pItemEdit = pData->pPanel->FindChildByNameT<DUIRichEdit>(TREEITEMEDIT, true); DMASSERT(pItemEdit);
			if (!pStaticText || !pItemEdit)
			{
				break;
			}
		
			pItemEdit->SetAttribute(L"text", pStaticText->m_pDUIXmlInfo->m_strText);
			pItemEdit->DM_SetVisible(true, true);
			pStaticText->DM_SetVisible(false, true);

			pItemEdit->m_EventMgr.SubscribeEvent(DM::DMEventRENotifyArgs::EventID, Subscriber(&DUIEffectTreeCtrl::OnTreeItemEditKillFocus, this));
			pItemEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&DUIEffectTreeCtrl::OnTreeItemEditInputReturn, this));
			hRet = DM_ECODE_OK;
		} while (false);		
		return hRet;
	}

	DMCode DUIEffectTreeCtrl::RenameTreeItem(DM::LPTVITEMEX pData, CStringW strNewName)
	{
		DMCode hRet = DM_ECODE_FAIL;
		do
		{
			if (!pData)
				break;

			DUIStatic* pStaticText = pData->pPanel->FindChildByNameT<DUIStatic>(TREESTATEXT, true); DMASSERT(pStaticText);
			if (!pStaticText)
			{
				break;
			}
			pStaticText->DV_SetWindowText(strNewName);
			hRet = DM_ECODE_OK;
		} while (false);
		return hRet;
	}

	bool DUIEffectTreeCtrl::HoverItem(HDMTREEITEM hItem, bool bEnsureVisible)
	{
		bool bRet = false;
		do
		{
			if (m_hHoverItem == hItem)
			{
				bRet = true;
				break;
			}

			if (NULL == hItem || DMTVI_ROOT == hItem)
			{
				break;
			}

			if (hItem&&bEnsureVisible)
			{
				EnsureVisible(hItem);
			}
			HDMTREEITEM hOldItem = m_hHoverItem;
			m_hHoverItem = hItem;
			if (hOldItem)
			{
				RedrawItem(hOldItem);
			}
			if (m_hHoverItem)
			{
				RedrawItem(m_hHoverItem);
			}

			bRet = true;
		} while (false);
		return bRet;
	}

	HDMTREEITEM DUIEffectTreeCtrl::InsertItem(DMXmlNode &XmlItem, HDMTREEITEM hParent/*=DMTVI_ROOT*/, HDMTREEITEM hInsertAfter/*=DMTVI_LAST*/, bool bEnsureVisible/*=false*/)
	{
		HDMTREEITEM hItem = (HDMTREEITEM)NULL;
		do 
		{
			hItem = DUITreeCtrlEx::InsertItem(XmlItem, hParent, hInsertAfter, bEnsureVisible); DMASSERT(hItem);
			if (NULL == hItem
				|| DMTVI_ROOT == hItem)
			{
				break;
			}
			DM::LPTVITEMEX pData = GetItem(hItem);
			if (!pData)
				break;
			DUIButton* pTreeAddBtn = pData->pPanel->FindChildByNameT<DUIButton>(TREEADDBUTTON, true);
			if (pTreeAddBtn)
			{
				pTreeAddBtn->m_EventMgr.SubscribeEvent(DMEventCmdArgs::EventID, Subscriber(&DUIEffectTreeCtrl::OnTreeAddBtnClicked, this));
			}
			DUICheckBox* pTreeEyeChk = pData->pPanel->FindChildByNameT<DUICheckBox>(TREECHECKNAME, true); DMASSERT(pTreeEyeChk);
			if (!pTreeEyeChk)
				break;
			pTreeEyeChk->m_EventMgr.SubscribeEvent(DMEventCmdArgs::EventID, Subscriber(&DUIEffectTreeCtrl::OnTreeEyeBtnClicked, this));
		} while (false);

		UpdateScrollRange();
		return hItem;
	}
		
	void DUIEffectTreeCtrl::OnLButtonDown(UINT nFlags, CPoint pt)
	{
		do 
		{
			HDMTREEITEM hItem = HitTest(pt);
			if (NULL == hItem || DMTVI_ROOT == hItem)
			{
				if (NULL == hItem && GetSelectedItem())
				{
					INSERTNEWOBSERVEACTION(new TreeSelectActionSlot(m_hSelItem, NULL, this));
					SelectItem(NULL); //lzlong  取消选择
				}
				break;
			}

			DM::LPTVITEMEX pData = GetItem(hItem);
			DUIButton* pTreeAddBtn = pData->pPanel->FindChildByNameT<DUIButton>(TREEADDBUTTON, true);
			if (pTreeAddBtn && pTreeAddBtn->HitTestPoint(pt))
			{
				SetMsgHandled(TRUE);//不需要传递下去了
				break;
			}

			DUICheckBox* pTreeEyeChk = pData->pPanel->FindChildByNameT<DUICheckBox>(TREECHECKNAME, true); DMASSERT(pTreeEyeChk);
			if (pTreeEyeChk && pTreeEyeChk->HitTestPoint(pt))
			{	
				SetMsgHandled(TRUE);//不需要传递下去了
				break;
			}

			m_hHoverItem = hItem;
			if (m_hHoverItem != m_hSelItem && m_hHoverItem)
			{
				INSERTNEWOBSERVEACTION(new TreeSelectActionSlot(m_hSelItem, m_hHoverItem, this));
				SelectItem(m_hHoverItem, false);
			}

			if (m_hHoverItem)
			{
				m_hCaptureItem = m_hHoverItem;
				ItemLButtonDown(m_hHoverItem, nFlags, pt);
			}
			SetMsgHandled(FALSE);
		} while (false);
	}

	void DUIEffectTreeCtrl::OnLButtonDbClick(UINT nFlags, CPoint pt)
	{
		do
		{
			bool bRenameItem = false;
			HDMTREEITEM hItem = HitTest(pt);
			if (NULL == hItem || DMTVI_ROOT == hItem)
			{
				break;
			}

			DM::LPTVITEMEX pData = GetItem(hItem);
			DUIButton* pTreeAddBtn = pData->pPanel->FindChildByNameT<DUIButton>(TREEADDBUTTON, true);
			if (pTreeAddBtn && pTreeAddBtn->HitTestPoint(pt))
				break;

			DUICheckBox* pTreeEyeChk = pData->pPanel->FindChildByNameT<DUICheckBox>(TREECHECKNAME, true); DMASSERT(pTreeEyeChk);
			if (pTreeEyeChk && pTreeEyeChk->HitTestPoint(pt))
				break;

			DUIRichEdit* pItemEdit = pData->pPanel->FindChildByNameT<DUIRichEdit>(TREEITEMEDIT, true); DMASSERT(pItemEdit);
			if (pItemEdit && pItemEdit->HitTestPoint(pt))
			{
				if (pData->lParam)
				{
					ObjTreeDataPtr lpData = (ObjTreeDataPtr)pData->lParam;
					if (EFFECTTEMPBTN_TRANSITIONSNODE == lpData->m_iEffectType)
					{
						RenameTreeItem(hItem);
						bRenameItem = true;
					}
				}
			}
			m_hHoverItem = hItem;
			if (m_hHoverItem && !bRenameItem)
			{
				Expand(m_hHoverItem, DMTVEX_TOGGLE);
			//	ItemLButtonDbClick(m_hHoverItem, nFlags, pt);
			}
		} while (false);	
		SetMsgHandled(TRUE);
	}

	HDMTREEITEM DUIEffectTreeCtrl::GetNextVisibleItem(HDMTREEITEM hItem)
	{
		if (hItem == DMTVI_ROOT)
		{
			return (HDMTREEITEM)m_hRootFirst;
		}

		if (NULL == hItem)
			return (HDMTREEITEM)NULL;
		DM::LPTVITEMEX pData = GetItem(hItem);
	
		HDMTREEITEM hRet = (HDMTREEITEM)NULL;
		if (pData && !pData->bCollapsed)
		{
			hRet = GetChildItem(hItem);
			if (hRet)
			{
				return hRet;
			}
		}
		HDMTREEITEM hParent = hItem;
		while (hParent)
		{
			hRet = GetNextSiblingItem(hParent);
			if (hRet)
			{
				return hRet;
			}
			hParent = GetParentItem(hParent);
		}
		return NULL;
	}

	HDMTREEITEM DUIEffectTreeCtrl::GetPrevVisibleItem(HDMTREEITEM hItem)
	{
		if (hItem == DMTVI_ROOT)
		{
			return (HDMTREEITEM)m_hRootFirst;
		}

		HDMTREEITEM hRet = (HDMTREEITEM)NULL;
		HDMTREEITEM hParent = hItem;
		hRet = GetPrevSiblingItem(hParent);
		if (hRet)
		{
			if (DMTVI_ROOT == hRet)
			{
				return (HDMTREEITEM)m_hRootFirst;
			}

			DM::LPTVITEMEX pData = GetItem(hRet);
			HDMTREEITEM hChild = GetChildItem(hRet, false);
			if (hChild && !pData->bCollapsed)
			{
				return hChild;
			}
			else
				return hRet;
		}
		else
			return GetParentItem(hParent);
		
		return NULL;
	}

	void DUIEffectTreeCtrl::OnKeyDown(TCHAR nChar, UINT nRepCnt, UINT nFlags)
	{
		HDMTREEITEM hItem = GetSelectedItem();
		do
		{
			if (NULL == hItem)
			{
				break;
			}

			if (VK_DOWN == nChar)
			{
				HDMTREEITEM hNextItem = GetNextVisibleItem(hItem);
				if (NULL == hNextItem)
					break;
				INSERTNEWOBSERVEACTION(new TreeSelectActionSlot(m_hSelItem, hNextItem, this));
				SelectItem(hNextItem);
			}
			else if (VK_UP == nChar)
			{
				HDMTREEITEM hPrevItem = GetPrevVisibleItem(hItem);
				if (NULL == hPrevItem)
					break;
				INSERTNEWOBSERVEACTION(new TreeSelectActionSlot(m_hSelItem, hPrevItem, this));
				SelectItem(hPrevItem);
			}
			else if (VK_LEFT == nChar)
			{
				Expand(hItem, DMTVEX_COLLAPSE);
			}
			else if (VK_RIGHT == nChar)
			{
				Expand(hItem, DMTVEX_EXPAND);
			}
			else if (VK_DELETE == nChar && GetSelectedItem())
			{
				ObjTreeDataPtr pData = (ObjTreeDataPtr)GetItemData(hItem);
				if (NULL == pData)
					break;

				if (EFFECTTEMPBTN_MAKEUPS == pData->m_iEffectType || EFFECTTEMPBTN_MAKEUPNODE == pData->m_iEffectType || EFFECTTEMPBTN_PARTNODE == pData->m_iEffectType || 
						EFFECTTEMPBTN_NOSE == pData->m_iEffectType || EFFECTTEMPBTN_BLUSH == pData->m_iEffectType || EFFECTTEMPBTN_EYESHADOW == pData->m_iEffectType ||
						EFFECTTEMPBTN_EYE == pData->m_iEffectType || EFFECTTEMPBTN_LIPV2 == pData->m_iEffectType || EFFECTTEMPBTN_FACETRANSPLANT == pData->m_iEffectType)
				{
					g_pMainWnd->HandleEffectTreeMenu(EFFECTTREEMENU_DELETE, hItem);
				}
			}
		} while (false);
		SetMsgHandled(FALSE);
	}

	void DUIEffectTreeCtrl::DrawItem(IDMCanvas* pCanvas, CRect& rc, HDMTREEITEM hItem)
	{
		do
		{
			if (NULL == hItem
				|| DMTVI_ROOT == hItem)
			{
				break;
			}

			CRect rcClient;
			DV_GetClientRect(rcClient);

			//1.绘制背景
			CRect rcItemBg(rcClient.left, rc.top, rcClient.right, rc.bottom);
			DUIWND_STATE iState = DUIWNDSTATE_Normal;
			if (hItem == m_hSelItem)
			{
				iState = DUIWNDSTATE_PushDown;
			}
			else if (hItem == m_hHoverItem)
			{
				iState = DUIWNDSTATE_Hover;
			}
			if (m_pItemBgSkin)
			{
				m_pItemBgSkin->Draw(pCanvas, rc, iState);
			}
			else if (!m_crItemBg[iState].IsTextInvalid())
			{
				pCanvas->FillSolidRect(rc, m_crItemBg[iState]);
			}


			int iXOffset = GetItemXOffset(hItem);

			// 2.绘制小三角和checkbox
			DM::LPTVITEMEX pData = GetItem(hItem);
			int iOffset = 0;
			if (m_pCheckSkin&&m_bCheckBox)//先绘checkbox
			{
				iOffset = m_szCheck.cx;
				CRect rcCheck(rc.left - m_szCheck.cx, rc.top, rc.left, rc.bottom);
				MeetRect(rcCheck, m_szCheck);
				int iImgState = IIF_STATE3(pData->dwCheckBoxState, 0, 1, 2);
				if (pData->iCheckValue == DMTVEXCheckBox_Checked)
				{
					iImgState += 3;
				}
				else if (pData->iCheckValue == DMTVEXCheckBox_PartChecked)
				{
					iImgState += 6;
				}

				m_pCheckSkin->Draw(pCanvas, rcCheck, iImgState);
			}

			if (pData->bHasChildren && m_pToggleSkin)
			{
				int iImgState = IIF_STATE3(pData->dwToggleState, 0, 1, 2);
				if (!pData->bCollapsed)
				{
					iImgState += 3;
				}
				m_pToggleSkin->GetStateSize(m_szToggle);
				CRect rcToggle(rc.left - iOffset - m_szToggle.cx + iXOffset, rc.top, rc.left - iOffset + iXOffset, rc.bottom);
				MeetRect(rcToggle, m_szToggle);
				m_pToggleSkin->Draw(pCanvas, rcToggle, iImgState);
			}

			// 绘制面板
			rc.right = rc.right > rcClient.right ? rcClient.right : rc.right;
			pData->pPanel->DrawItem(pCanvas, rc);
		} while (false);
	}

	int DUIEffectTreeCtrl::GetItemWidth(HDMTREEITEM hItem)
	{
		int iWidth = 0;
		do
		{
			if (NULL == hItem
				|| DMTVI_ROOT == hItem)
			{
				break;
			}
			DM::LPTVITEMEX pData = GetItem(hItem);
			iWidth = pData->iWidth;

			//lzlong
			CRect rcClient;
			DV_GetClientRect(rcClient);
			if (rcClient.IsRectEmpty())
			{
				break;
			}
			int iXOffset = GetItemXOffset(hItem);
			iWidth = rcClient.Width() - iXOffset;
		} while (false);
		return iWidth;
	}
	void DUIEffectTreeCtrl::UpdateScrollRange()
	{
		int iHei = GetTotalHeight();
		CSize szView(0, iHei);
		SetRangeSize(szView);
	}

	void DUIEffectTreeCtrl::UpdateVisibleMap()
	{
		DMMapT<HDMTREEITEM, CRect>::RemoveAll();
		CRect rcClient;
		DV_GetClientRect(rcClient);
		if (rcClient.IsRectEmpty())
		{
			return;
		}

		HDMTREEITEM hItem = GetNextItem(DMTVI_ROOT);// 从根结点开始向下查找
		int iTotalHei = 0; int iXOffset = 0;
		while (hItem)
		{
			if (NULL == hItem || DMTVI_ROOT == hItem)
			{
				break;
			}

			DM::LPTVITEMEX pData = GetItem(hItem);
			if (pData->bVisible)
			{
				int iwidth = GetItemWidth(hItem);
				CRect rcLayout(0, 0, rcClient.Width()/*GetItemWidth(hItem)*/, pData->iHeight);
				pData->pPanel->DM_FloatLayout(rcLayout);

				if ((iTotalHei >= m_ptCurPos.y && iTotalHei < m_ptCurPos.y + rcClient.Height())
					|| (iTotalHei + pData->iHeight >= m_ptCurPos.y && iTotalHei + pData->iHeight < m_ptCurPos.y + rcClient.Height())
					|| (iTotalHei <= m_ptCurPos.y && iTotalHei + pData->iHeight >= m_ptCurPos.y + rcClient.Height())
					)
				{
					iXOffset = GetItemXOffset(hItem);
					CRect rcItem(0/*iXOffset*/, iTotalHei, iXOffset + GetItemWidth(hItem), iTotalHei + pData->iHeight);// 在大平面的坐标，以大平面左上角为原点
					rcItem.OffsetRect(rcClient.TopLeft() - m_ptCurPos);// 转换成rcClient所在的坐标系坐标
					if (m_hSelItem != hItem)
					{
						pData->pPanel->OnSetFocusWnd(NULL);
					}
					DMMapT<HDMTREEITEM, CRect>::AddKey(hItem, rcItem);
				}
				iTotalHei = iTotalHei + pData->iHeight;
				if (iTotalHei >= m_ptCurPos.y + rcClient.Height())// 总高度已超过可视区
				{
					break;
				}
			}

			if (pData->bCollapsed)
			{// 跳过被折叠的项
				HDMTREEITEM hChild = GetChildItem(hItem, false);
				while (hChild)
				{
					hItem = hChild;
					hChild = GetChildItem(hItem, false);
				}
			}
			hItem = GetNextItem(hItem);
		}
	}

	bool DUIEffectTreeCtrl::HasEffectTypeRootNode(int type)
	{
		DM::HDMTREEITEM hitem = GetRootItem();
		while (hitem)
		{
			ObjTreeDataPtr pData = (ObjTreeDataPtr)GetItemData(hitem);
			if (!pData || !pData->IsValid())
			{
				break;
			}
			if (pData->m_iEffectType == type)
				return true;

			hitem = GetNextSiblingItem(hitem);
		}

		return false;
	}

	void DUIEffectTreeCtrl::OnNodeFree(DM::LPTVITEMEX &pItemData)
	{
		m_DeletedItemsDataArr.Add(pItemData);
		m_DeletedItemsLparamArr.Add(pItemData ? pItemData->lParam : NULL);
		if (pItemData->lParam)
		{
			ObjTreeDataPtr pData = (ObjTreeDataPtr)pItemData->lParam;
			if (pData->m_pJsonParser && pData->m_pJsonParser != &g_pMainWnd->m_JSonMainParser)
			{
				g_pMainWnd->m_JSonMainParser.MarkDataDirty(true);
				pData->m_pJsonParser->DestoryParser();
			}
			if (pData)
			{
				delete pData;
				pItemData->lParam = NULL;
			}
		}

		DUITreeCtrlEx::OnNodeFree(pItemData);
	}

	bool DUIEffectTreeCtrl::IsItemStillExist(const DM::LPTVITEMEX &pItemData, LPARAM lp)
	{
		if (!pItemData)
			return false;
		int nCount = (int)m_DeletedItemsDataArr.GetCount();
		for (int i = 0; i < nCount; i++)
		{
			if (pItemData == m_DeletedItemsDataArr[i] && m_DeletedItemsLparamArr[i] == lp)
				return false;
		}
		return true;
	}

	int DUIEffectTreeCtrl::ItemHitTest(HDMTREEITEM hItem, CPoint &pt)
	{
		int iHitTestBtn = DMTVEXBtn_None;
		do
		{
			if (NULL == hItem || DMTVI_ROOT == hItem)
			{
				break;
			}
			DM::LPTVITEMEX pData = GetItem(hItem);
			if (!pData->bVisible)
			{
				break;
			}

			if (pData->bHasChildren && m_pToggleSkin)
			{
				int iXOffset = GetItemXOffset(hItem);
				CRect rcItem;
				if (!GetItemRect(hItem, rcItem))
				{
					break;
				}
				
				m_pToggleSkin->GetStateSize(m_szToggle);
				CRect rcToggle(rcItem.left - m_szToggle.cx + iXOffset, rcItem.top, rcItem.left + iXOffset, rcItem.bottom);
				MeetRect(rcToggle, m_szToggle);
				CPoint Pt = pt + rcItem.TopLeft();
				if (rcToggle.PtInRect(Pt))
				{
					iHitTestBtn = DMTVEXBtn_Toggle;
					break;
				}
			}
		} while (false);
		return iHitTestBtn;
	}

	void DUIEffectTreeCtrl::SetTreeItemEyeBtnChk(HDMTREEITEM hItem, bool bVisible, bool bdisableBtn)
	{
		do
		{
			if (NULL == hItem || DMTVI_ROOT == hItem)
			{
				break;
			}
			
			DM::LPTVITEMEX pData = GetItem(hItem);
			DUICheckBox* pTreeEyeChk = pData->pPanel->FindChildByNameT<DUICheckBox>(TREECHECKNAME, true); DMASSERT(pTreeEyeChk);
			DUIStatic* pStaticText = pData->pPanel->FindChildByNameT<DUIStatic>(TREESTATEXT, true); DMASSERT(pStaticText);
			if (!pTreeEyeChk || !pStaticText)
				break;

			bool bEyeChk = bVisible && !bdisableBtn;
			pTreeEyeChk->DM_SetCheck(bVisible);
			pTreeEyeChk->SetAttribute(XML_BDISABLE, bdisableBtn ? L"1" : L"0");
			
			pStaticText->SetAttribute(L"clrtext", bEyeChk ? L"rgba(ff,ff,ff,dd)" : L"rgba(aa,aa,aa,dd)");
			UpdateEye(hItem, bEyeChk);
			
			ObjTreeDataPtr pItemData = (ObjTreeDataPtr)GetItemData(hItem);
			if (pItemData && pItemData->m_pJsonParser)
			{
				if (0 != _wcsicmp(pItemData->m_pJsonParser->V_GetClassName(), EDFaceMorphParser::GetClassName())) //EDFaceMorphParser 没有enable的属性 所以排除它
				{
					pItemData->m_pJsonParser->SetJSonAttributeBool(EDAttr::EDPartsNodeParserAttr::BOOL_enable, bEyeChk);
				}					
			}
			
			HDMTREEITEM hChild = GetChildItem(hItem);
			while (hChild)
			{
				SetTreeItemEyeBtnChk(hChild, IsItemEyeChk(hChild), !bVisible);
				hChild = GetNextSiblingItem(hChild);
			}
		} while (false);
	}

	DMCode DUIEffectTreeCtrl::UpdateEye(HDMTREEITEM hItem, bool bVisible)
	{
		do
		{
			if (NULL == hItem || DMTVI_ROOT == hItem)
			{
				break;
			}
			ObjTreeDataPtr pData = (ObjTreeDataPtr)GetItemData(hItem);
			if (NULL == pData || NULL == pData->m_pDUIWnd || !pData->IsValid())
			{
				break;
			}
			if (pData->m_pRootWnd == pData->m_pDUIWnd) //如果是root窗口 不隐藏
				break;
			pData->m_pDUIWnd->DM_SetVisible(bVisible, true);
		} while (false);
		return DM_ECODE_OK;
	}

}//end namespace ED