// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	duireferenceline.h
// File mark:   
// File summary:参考点的参考线
// Author:		
// Edition:     1.0
// Create date: 2019-4-8
// ----------------------------------------------------------------

#pragma once

class DUIReferenceLine : public DUIStatic
{
	DMDECLARE_CLASS_NAME(DUIReferenceLine, DUINAME_REFERENCELINE, DMREG_Window)

public:
	DUIReferenceLine();
	~DUIReferenceLine();

	DM_BEGIN_MSG_MAP()
		DM_MSG_WM_PAINT(DM_OnPaint)
	DM_END_MSG_MAP()

	void SetDotPoint(POINT pt1, POINT pt2);
	void DM_OnPaint(IDMCanvas* pCanvas);

private:
	DMColor m_Clr;
	DMColor m_BlackClr;
	POINT	m_point[2];
};

