// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	Data.h
// File mark:   
// File summary:绑定数据定义
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-2-28
// ----------------------------------------------------------------
#pragma once

/// <summary>
///		objtree每一项绑定的数据
/// </summary>
 class ObjTreeData
 {
 public:
	 ObjTreeData(DUIRoot* pRootWnd, DUIWindowPtr pDUI, EffectTempletType iEffectType, HDMTREEITEM hTreeItem, EDJSonParserPtr pJSonParser = NULL);
	 ~ObjTreeData();
	 void SetData(DUIRoot* pRootWnd, DUIWindowPtr pDUI, EffectTempletType iEffectType, HDMTREEITEM hTreeItem, EDJSonParserPtr pJSonParser = NULL);
	 bool IsValid() const;

 public:
	 EDJSonParserPtr					  m_pJsonParser;		///< JsonParser绑定的指针
	 DUIRoot*							  m_pRootWnd;			///< 自身所在编辑主窗口
	 DUIWindowPtr                         m_pDUIWnd;			///< 自身所在窗口
	 EffectTempletType					  m_iEffectType;
	 bool								  m_bAttrlistExp[10];	///< 参数信息属性信息面板是否展开；
 };

typedef ObjTreeData* ObjTreeDataPtr;

class TransitionTreeData
{
public:
	TransitionTreeData(int tag, EDJSonParserPtr pJSonParser = NULL);
public:
	EDJSonParserPtr						m_pJSonParser;	///< 底层数据绑定
	int									m_treeTag;
};

typedef TransitionTreeData* TransitionTreeDataPtr;
